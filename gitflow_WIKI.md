# Git Flow WIKI

## Naming of branches:

- `feature/some_feature`
- `hotfix/some_hotfix`
- `bugfix/some_hotfix`

## Types of branches:

- `main` - Production branch. We push final changes from `Dev` and `hotfixes` to this branch (`git flow release start 0.1.0`);
- `develop` - Staging branch. We push approved `features` to this branch (`git flow feature publish <my-feature>`);
- `feature` - Branch with new features. We create merge requests from this branch to `develop` branch (`git flow feature start <my-feature>`);
- `bugfix` - Branch with new bugfixes. We create new branch from `develop` branch. We merge from this branch to `develop` branch directly (`git flow bugfix start <bugfix-name>` - `git flow bugfix finish <bugfix-name>`);
- `hotfix` - Branch with new hotfixes. We create new branch from `main` branch. We merge from this branch to `develop` branch and to `main` branch directly (`git flow hotfix start 0.1.1` - `git flow hotfix finish '0.1.1'`);

## Merge requests naming:

- `| jira_task_number | branch_type (feature/hotfix/fix) | short task description |`

### Examples:

- `| JR-08 | feature | add model for user |`
- `| JR-09 | hotfix | fix registration mechanics |`
- `| JR-10 | bugfix | fix likes mechanics |`

## Description of merge request

```md
    ## Task link

    ## What has been changed?

    ## Important notes

    ## Self-checks

    - [ ] App is working
    - [ ] Approved by 2 team-mates
    - [ ] Approved by mentor
```

## When I can merge to `develop`:

- All code is lintered by rubocop
- All tests passed
- I have 2 approves from team-mates
- I have approve from mentor

## When `develop` merges into `main`:

- Only owner can merge into `main` branch.
- Owner merges into `main` when sprint is done.
- `develop` branch has zero bugs.

## Labels:

- `work in progress`
- `needs review`
- `request changes`
- `needs mentors review`

## Main rules:

- Approve: after 2 approves from team-mates, ask for mentor review in out teams-chat
- Squash commits when you merge MR
- Delete branch after MR
